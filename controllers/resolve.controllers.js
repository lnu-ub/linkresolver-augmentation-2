const uresolver = require('../utils/uresolver.util.js');
const crossref = require('../utils/crossref.util.js');
const LibrisUtil = require('../utils/libris.util.js');
const IsbnUtil = require('../utils/isbn.util.js');
const parseString = require('xml2js').parseString;

const OpenURLResolver = async (req, res, next) => {
    const rft = req.state.referent;

    console.log( 'Resolve!!' );

    // Get metadata
    const contextObject = await req.state.contextObjectPromise;
    const contextServices = await req.state.contextServicesPromise;
    const pubmed = await req.state.pubmedPromise;
    const unpaywall = await req.state.unpaywallPromise;


    // Compare metadata to validate
    // End validation of metadata

    let unpaywallData = unpaywall ? JSON.parse(unpaywall) : null;
    let prioritizedService = contextServices.length > 0 ? contextServices[0] : null; // May we prioritize another way?

    crossref.search(rft)
        .then( result => {
            console.log( result );
        });

    
    // Choose where to redirect
    if ( unpaywallData &&  unpaywallData.is_oa && !prioritizedService ) {
        res.redirect( unpaywallData.best_oa_location.url );
    }
    else if ( prioritizedService ) {
        if ( prioritizedService.keys.Is_free || unpaywallData.is_oa ) {
            res.redirect( unpaywallData.best_oa_location.url );
        }
        else {
            // No OA available
            res.send(rft);
        }
    }
    else {
        // Neither OA or prioritized service

        if ( 'dissertation' === rft.genre ) {
            const swepub = new LibrisUtil.LibrisXSearch('swepub');
            
            // Check country of publication firstly, then the language
            if ( rft.isbn ) {
                const isbn = new IsbnUtil.Isbn(rft.isbn);

                if ( 'sweden' == isbn.countryOfPublication.toLowerCase() ) {
                    swepub.getFulltextLinks( isbn.isbn )
                        .then( (links) => {
                                if ( links.length === 1 ) {
                                    res.redirect( links[0].url );
                                }
                                else if ( links.length > 1 ) {
                                    console.log('dissertation with several possible links');
                                    res.send(rft);
                                }
                        })
                        .catch((error) => {
                            console.error(error);
                            res.send(rft);
                        });
                }
            }
            else if ( rft.btitle ) {
                import('franc')
                    .then((module) => {
                        const { franc } = module;

                        if ( ['swe', 'nob', 'dan'].includes( franc(rft.btitle) ) ) {
                            swepub.getFulltextLinks(`TITLE:(${rft.btitle}) YEAR:(${rft.year})`)
                                .then( (links) => {
                                    if ( links.length === 1 ) {
                                        res.redirect( links[0].url );
                                    }
                                    else if ( links.length > 1 ) {
                                        console.log('dissertation with several possible links');
                                        res.send(rft);
                                    }
                                })
                                .catch((error) => {
                                    console.error(error);
                                    res.send(rft);
                                });
                        }
                        else {
                            res.send(rft);
                        }
                    })
                    .catch((error) => {
                        console.error( error );
                        console.error('Failed');
                    });
            }
        }
        else {
            res.send(rft);
        }
    }

    

};

const PMIDResolver = (req, res, next) => {
    console.log( req.params );
    res.render('index', { title: 'PMID resolver' });
};

const DOIResolver = (req, res, next) => {
    console.log( req.params );
    res.render('index', { title: 'DOI resolver' });
};

module.exports = {
    OpenURLResolver, PMIDResolver, DOIResolver,
}
