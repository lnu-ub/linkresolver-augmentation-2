var express = require('express');
const checkOpenUrl = require('../middleware/checkOpenUrl.middleware.js');
const pubmed = require('../middleware/pubmed.middleware.js');
const subscriptions = require('../middleware/subscriptions.middleware.js');
const unrestricted = require('../middleware/unrestricted.middleware.js');
const resolveControllers = require('../controllers/resolve.controllers.js');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Link Resolver Augmenation 2' });
});

router.get('/openurl', checkOpenUrl, pubmed, subscriptions, unrestricted, resolveControllers.OpenURLResolver );
router.get('/pmid/:PMID', pubmed, resolveControllers.PMIDResolver );
router.get('/doi/:prefix*', resolveControllers.DOIResolver );

module.exports = router;
