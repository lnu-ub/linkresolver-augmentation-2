/* jshint esversion: 6 */
const Eutils = require('./eutils.js');
const https = require('https');

var Esearch = function(retmode = 'JSON') {
    this._eutils = new Eutils('esearch', {retmode: retmode});
    this._eutils.options.method = 'POST';
};

Esearch.prototype.searchPmc = function(issn, volume, issue, spage, aulast) {
    let par = {
        term: `${issn}[Jour] AND ${volume}[volume] AND ${issue}[issue] AND ${spage}[page] AND ${aulast}[author]`,
        db: 'pmc'
    };

    this._eutils.addPar(par);

    return new Promise((resolve, reject) => {
        let req = https.request(this._eutils.options, (res) => {
            let data = '';
            
            res.on('data', (chunk) => {
                data += chunk;
            });
            
            res.on('end', () => {
                let parsedData = JSON.parse(data);

                console.log(parsedData);

                if (parsedData.esearchresult && 1 == parsedData.esearchresult.count) {
                    resolve(parsedData.esearchresult.idlist[0]);
                }
                else {
                    reject('No or more than one esearchresult');
                }
            });
        }).on('error', (err) => {
            console.log("Error: " + err.message);
        });

        req.end();
    });
};

module.exports = Esearch;
