// https://www.ncbi.nlm.nih.gov/pmc/tools/id-converter-api/
/*************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-pmc',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
const querystring = require('querystring');

var confFileName = '../config/'+context+'.js';
delete require.cache[require.resolve(confFileName)];
var conf = require(confFileName);
log.debug('pmc utils reading config from '+confFileName);

var PmcUtils = function(api, par) {
    /* service-root: https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/ */
    /* parametrar: ids, idtype, format */
    /*https://www.ncbi.nlm.nih.gov/pmc/utils/idconv/v1.0/?tool=my_tool&email=m&ids=10.1093/nar/gks1195*/
    let path = {
        base: '/pmc/utils/',
        suffix: '&tool='+conf.eutils.tool+'&email='+conf.admin.email,
    }

    this.options.path =
        path.base +
        api + '/?' + querystring.stringify(par) +
        path.suffix;
}

PmcUtils.prototype.options = {
    hostname: 'www.ncbi.nlm.nih.gov',
    method: 'GET',
}

module.exports = PmcUtils;
