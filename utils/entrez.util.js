/* The Entrez Programming Utilities (E-utilities) are a set of nine server-side programs that provide a stable interface into the Entrez query and database system at the National Center for Biotechnology Information (NCBI). */
const https = require('https');
const parseString = require('xml2js').parseString;

class Pubmed {
    constructor(tool, email) {
        this.entrez = new Entrez(tool, email);

    }
    // should be a promise
    fetch(pmid) {
        return new Promise((resolve, reject) => {
            (async () =>  {
                try {
                    // Throw error if several PMIDs, validate 
                    // Efetch returns XML
                    const xmlString = await this.entrez.efetch('pubmed', 'xml', pmid);
                    console.log( 'efetch' );

                    parseString(xmlString, (err, result) => {
                        console.log( result );
                        // Efetch supports several IDs,
                        // however in this context we only supports a single PMID
                        const pubmedArticle = result.PubmedArticleSet.PubmedArticle[0];
                        const medlineCitation = pubmedArticle.MedlineCitation[0];
                        // To be used later
                        this.article = medlineCitation.Article[0];
                        this.journal = medlineCitation.MedlineJournalInfo[0];
                        return resolve(pubmedArticle);
                    });
                }
                catch (err) {
                    console.error(err);
                    return reject(err);
                }
            })();
        });
    }
    get spage() {
        return this.article.Pagination[0].StartPage[0];
    }
    get epage() {
        return this.article.Pagination[0].EndPage[0];
    }
}

class Entrez {
    constructor(tool, email) {
        this.tool = tool;
        this.email = email;
        this.host = 'eutils.ncbi.nlm.nih.gov';
        this.base = '/entrez/eutils';
    }
    efetch( db, retmode, id ) {
        // Either a single UID or a comma-delimited list of UIDs may be provided.
        return new Promise((resolve, reject) => {
            let options = {
                method: 'GET',
                hostname: this.host,
                path: `${this.base}/efetch?db=${db}&retmode=${retmode}&id=${id}`
            }

            const req = https.request(options, (res) => {
                let data = '';

                res.on('data', (chunk) => {
                    data += chunk;
                });

                res.on('end', () => {
                    // resolve with data
                    resolve(data);
                });
            });

            req.end();

            req.on('error', (e) => {
                reject(e);
            });
        });
    }
    esearch() {
    }
    ecitmatch() {
    }
    elink() {
    }
    idconv() {
    }
    pmc() {
    }
}

module.exports = {
    Pubmed
}
