const https = require('https');

/* API usage and rate limits: Be aware of the Unpaywall API's usage policies and rate limits. Ensure that you're adhering to their terms and conditions and any rate limits they impose to avoid any issues with your requests. */

class Unpaywall {
    constructor() {
        this.options = {
            hostname: 'api.unpaywall.org',
            path: '/v2/'
        }
    }
    request(doi) {
        return new Promise((resolve, reject) => {
            this.options.path = this.options.path + doi + '?email=jakob.nylinnilsson@lnu.se';
            
            const reqByDoi = https.request(this.options, (res) => {
                let data = '';

                res.on('data', (chunk) => {
                    data += chunk;
                });

                res.on('end', () => {
                    resolve( data );
                });
            });

            reqByDoi.end();

            reqByDoi.on('error', (e) => {
                reject(e);
            });
        });
    }
}

module.exports =  new Unpaywall();
