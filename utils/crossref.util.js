const https = require('https');

class Crossref {
    constructor() {
        this.options = {
            hostname: 'api.crossref.org',
            headers: {
                'User-Agent': 'Alma Link Resolver Augmentation/'+process.env.npm_package_version+' (mailto:jakob.nylinnilsson@lnu.se)'
            }
        }
    }
    search(rft) {
        let options = Object.assign({}, this.options);
        
        return new Promise((resolve, reject) => {
            let items = [];
            
            // Search volume, year, issue and pages
            if ( rft.volume && rft.date && rft.issue && rft.spage && rft.epage ) {
                const d = new Date(rft.date);
                let query = `Vol. ${rft.volume}(${d.getFullYear()}):${rft.issue}`;
                let path = `/journals/${encodeURIComponent(rft.issn)}/works?query.bibliographic=${encodeURIComponent(query)}&select=volume,issue,page,DOI`;

                this.options.path = path;

                const reqCrossref = https.request(this.options, (res) => {
                    let data = '';

                    res.on('data', (chunk) => {
                        data += chunk;
                    });

                    res.on('end', () => {
                        let result = JSON.parse(data);
                        result.message.items.forEach( i => {
                            if ( i.issue == rft.issue ) {
                                let crossrefPages = i.page.match(/^(\d*)-(\d*)$/);

                                // Check if article is part of a bigger article
                                if ( crossrefPages[1] <= rft.spage && rft.epage <= crossrefPages[2] ) {
                                    items.push(i);
                                }
                            }
                        });

                        resolve(items);
                    })
                });

                reqCrossref.end();

                reqCrossref.on('error', (e) => {
                });
            }
        });
    }
    metadata(doi) {
        return new Promise((resolver, reject) => {
        });
    }
}

module.exports = new Crossref();
