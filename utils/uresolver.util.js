const https = require('https');
const parseString = require('xml2js').parseString;

class Uresolver {
    constructor() {
        this.options = {
            method: 'GET',
            hostname: 'gslg-lnu.userservices.exlibrisgroup.com',
            path: '/view/uresolver/46GSLG_VAXJO/openurl-lnu?svc_dat=CTO'
        }
        this.parseXml = (xml) => {
            console.log(xml);
            let data = {}
            parseString( xml, (err, result) => {
                data.contextObject = result.uresolver_content.context_object;
                data.contextServices = result.uresolver_content.context_services;
                
                if ( data.contextServices.length == 1 && data.contextServices[0] === '' ) {
                    data.contextServices.length = 0;
                }
                else {
                    data.contextServices = data.contextServices[0].context_service;
                    data.contextServices.forEach( (contextService) => {
                        let keys = {}

                        contextService.keys[0].key.forEach( k => {
                            if ( 'undefined' !== typeof k._ ) {
                                if ( k.$.id.startsWith('Is_') ) {
                                    keys[k.$.id] = !! + k._;
                                }
                                else if ( k._ === 'true' || k._ === 'false' ) {
                                    keys[k.$.id] = k._ === 'true';
                                }
                                else if ( k._ === 'yes' || k._ === 'no' ) {
                                    keys[k.$.id] = k._ === 'yes';
                                }
                                else {
                                    keys[k.$.id] = k._;
                                }
                            }
                        });

                        contextService.keys = keys;
                        
                        contextService.resolution_url = contextService.resolution_url.toString();
                        
                        if ( contextService.target_url )
                            contextService.target_url = contextService.target_url.toString();
                    });
                }
            });
            return data;
        }
    }
    request(queryString) {
        return new Promise( (resolve, reject) => {
            this.options.path = this.options.path + `&${queryString}`;
            
            const reqSubs = https.request(this.options, (res) => {
                let xmlData = '';

                res.on('data', (chunk) => {
                    xmlData += chunk;
                });

                res.on('end', () => {
                    let data = this.parseXml(xmlData);
                    this.uresolverContent = data;
                    resolve( this.uresolverContent );
                });
            });

            reqSubs.end();

            reqSubs.on('error', (e) => {
                reject( e);
            });

        });
    }
    get contextObject() {
        return this.uresolverContent.contextObject;
    }
    get contextServices() {
        return this.uresolverContent.contextServices;
    }
    getValue(key) {
        let kev = this.contextObject[0].keys[0].key.find( (k) => {
            return k.$.id === key;
        });
        return kev._;
    }
}

module.exports = new Uresolver();

