// A KEV OpenURL may contain only one ContextObject
function ContextObject( query ) {
    // Referent: Information about the referenced item
    this.rft = new Referent( query );
    // Other possible entities: ReferringEntity, Requester, Resolver, ServiceType, Referrer
}

class Referent {
    constructor( query ) {
        console.log( query );
        this.val_fmt = query['rft_val_fmt']; // conditionally required
    
        this.id = query.rft_id;
        // also galeid, pqid ?

        this.genre = query['rft.genre']; // conditionally required

        // Title
        this.atitle = query['rft.atitle'];
        this.title = query['rft.title'];
        this.btitle = query['rft.btitle'];
        this.jtitle = query['rft.jtitle'];

        // Author
        this.au = query['rft.au'];
        this.aulast = query['rft.aulast'];
        this.aufirst = query['rft.aufirst'];
        this.auinit = query['rft.auinit'];
        this.auinit1 = query['rft.auinit1'];
        this.auinitm = query['rft.auinitm'];
        this.aucorp = query['rft.aucorp'];
        this.degree = query['rft.degree'];
    
        // Date and volume
        this.date = query['rft.date'];
        this.volume = query['rft.volume'];
        this.issue = query['rft.issue'];
    
        // Pages
        this.spage = query['rft.spage'];
        this.epage = query['rft.epage'];
        this.pages = query['rft.pages'];

        // Standard numbers
        /* This is strange. ISSN should be the ISSN of the publication
           so what is EISSN? */
        this.issn = query['rft.issn'];
        this.eissn = query['rft.eissn'];
        this.isbn = query['rft.isbn'];
    
        // Publisher
        this.pub = query['rft.pub'];
        this.publisher = query['rft.publisher'];
        this.inst = query['rft.inst'];
        this.place = query['rft.place'];

        // Data 
        this.dat = query['rft_dat'];
    }
    get pmid() {
        if ( this.id ) {
            let rft_pmid = this.id.find( (rft_id) => {
                return rft_id.startsWith('info:pmid');
            });

            return parseRftId(rft_pmid).identifier;
        }
        else {
            return null;
        }
    }
    get doi() {
        if ( this.id ) {
            let rft_doi = this.id.find( (rft_id) => {
                return rft_id.startsWith('info:doi');
            });

            return parseRftId(rft_doi).identifier;
        }
        else {
            return null;
        }
    }
    get atitleIdenticalToTitle() {
        return this.atitle == this.title
    }
    get queryString() {
        return new URLSearchParams(this).toString();
    }
}

function parseRftId(rft_id) {
    let id = rft_id.split(':')[1];

    return {
        namespace: id.slice(0, id.indexOf('/')),
        identifier:  id.slice(id.indexOf('/')+1)
    };
}

module.exports = {
    Referent,
}



