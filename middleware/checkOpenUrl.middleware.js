/* Check the OpenURL, is it a reasonable request? */
const OpenURLUtil = require('../utils/openurl.util.js');

module.exports = (req, res, next) => {
    console.log('Checking OpenURL');
    try {
        let state = {};
        let referent = new OpenURLUtil.Referent( req.query );

        console.log( referent );

        // Check if publication title and article title are the same
        if ( referent.genre === 'article' && referent.atitleIdenticalToTitle ) {
            throw 'Same article and publication title';
        }
        else {
            state.referent = referent;
            req.state = state;
        }
        next();
    } catch (err) {
        console.log(err);
        res.status(500).send(err);
    }
};
