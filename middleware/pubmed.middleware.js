const EntrezUtil = require('../utils/entrez.util.js');

module.exports = (req, res, next) => {
    try {
        const pubmed = new EntrezUtil.Pubmed('medbib-linkresolver', 'jakob.nylinnilsson@lnu.se');
        const rft = req.state.referent;

        console.log( 'get pubmed data');

        if ( rft.pmid ) {
            console.log( 'fetch pubmed' );
            req.state.pubmedPromise = pubmed.fetch( rft.pmid );
        }
        
        next();
    }
    catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
};
