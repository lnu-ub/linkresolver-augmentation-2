// Gets the OA status of a publication
// Unpaywall is the preferred source
const UnpaywallUtil = require('../utils/unpaywall.util.js');

module.exports = (req, res, next) => {
    try {
        if ( req.state.referent.doi ) {
            console.log('Check for OA on', req.state.referent.doi );
            req.state.unpaywallPromise = UnpaywallUtil.request( req.state.referent.doi );
        }

        //console.log('Check for OA on', req.state.referent.pmid );
        next();
    } catch (err) {
        console.error( err );
        res.status(500).send(err);
    }
};
