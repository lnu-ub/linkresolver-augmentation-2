// Gets context object form the link resolver which holds information about our subscriptions
const https = require('https');
const uresolver = require('../utils/uresolver.util.js');

module.exports = async (req, res, next) => {
    try {
        console.log('Get subscriptions');

        await uresolver.request( req.state.referent.queryString );
        req.state.contextServicesPromise = Promise.resolve( uresolver.contextServices );
        req.state.contextObjectPromise = Promise.resolve( uresolver.contextObject ); // This need to be validated

        next();
    }
    catch (err) {
        console.log( err );
        res.status(500).send(err);
    }
};
